'use strict';

angular.module('app',['ui.router','lk.Player','lk.Resource','lk.ActivityModels'])

.config(function ($stateProvider, $urlRouterProvider,$locationProvider,$sceProvider) {

  $sceProvider.enabled(false);

  $urlRouterProvider.otherwise('/courses');

  $stateProvider
  .state('courses', {
    url: '/courses',
    views : {
      'layout':{
        templateUrl: 'views/layout-default.html'
      },
    }
  })
  .state('courses.view', {
    url: '/:courseId',
    views : {
      'content':{
        templateUrl: 'views/lesson.html',
      }
    }
  })
  .state('courses.project', {
    url: '/:courseId/:lessonId',
    views : {
      'content':{
        templateUrl: 'views/player.html',
      }
    }
  })
  .state('courses.project.play', {
    url: '/play/:activityId',
    views : {
      'content':{
        templateUrl: 'views/lesson.html'
      }
    }
  })
  .state('users', {
    url: '/users',
    views : {
      'container':{
        templateUrl: 'views/users.html'
      },
    }
  })
  .state('users.view', {
    url: '/:userId',
    views : {
      'content':{
        templateUrl: 'views/user.html'
      },
    }
  });
})
.run(['$rootScope', '$stateParams',
  function ($rootScope, $stateParams) {
    //$rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
  }]);
