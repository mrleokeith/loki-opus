'use strict';

angular.module('app')


.controller('MainCtrl', ['$scope','Evaluator','Resource','Activity','Agent','Interaction','Experience','$templateCache',function ($scope,Evaluator,Resource,Activity,Agent,Interaction,Experience,$templateCache) {

	var url = 'http://localhost/learninator/api';

	var Users = new Resource(url,'User',new Agent());
	var Activities = new Resource(url,'ModuleNode',new Activity());
	var Interactions = new Resource(url,'Submission',new Interaction(Users,Activities));
	var Experiences = new Resource(url,'ModuleNode',new Experience(Users,Interactions,Evaluator,23));


	$scope.interactions = Interactions.query({user_id:23,type:'Assessment'},{},function(r){
		$scope.$interactionsLoaded = true;
	});

	$scope.$on('$stateChangeSuccess',function(){
		$scope.$stateLoaded = true;
	});


	$scope.$watch('$stateLoaded&&$interactionsLoaded',function(){
		if($scope.$stateParams.lessonId) {
			if($scope.activity&&$scope.activity.stop) {
				$scope.activity.stop();
			}
			$scope.activity = Experiences.get({id:$scope.$stateParams.lessonId},{},function(r){
				r.$shouldPlay = true;
			});
		}
		else if($scope.$stateParams.courseId) {
			$scope.lesson = Experiences.get({id:$scope.$stateParams.courseId});
		}
		else {
			$scope.course = Experiences.get(224929);
		}
	});

	// $http.get('data/activities.json').success(function(response){
	// 	$scope.course = Player.load(response);
	// });


}])


.service('Wikipedia',['$http','$sce',function($http,$sce){
	var data = {
		html:'',
		img:''
	};
	return {
		get:function(titles){
			data.html = '<div class="loading"><div></div></div>';
			$http({
				method:'JSONP',
				url:'http://en.wikipedia.org/w/api.php',
				params:{
					action:'query',
					prop:'revisions',
					rvprop:'content',
					rvparse:true,
					rvsection:0,
					titles:titles,
					format:'json',
					callback:'JSON_CALLBACK',
					indexpageids:true,
					redirects:true
				}
			}).
			success(function(response){
				console.log(response);
				var pageId = response.query.pageids[0];
				data.html = $sce.trustAsHtml(response.query.pages[pageId].revisions[0]['*']);
				console.log(data);
			}).
			error(function(response,error){
				console.log(response);
				console.log(error);
			});
			return data;
		}
	}
}]);